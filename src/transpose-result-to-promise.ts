import type { Result } from './result.ts'
import { ok } from './ok.ts'
import { err } from './err.ts'

const transposeResultToPromise = async <SuccessType, ErrorType>(
  result: Result<Promise<SuccessType>, ErrorType>
): Promise<Result<SuccessType, ErrorType>> =>
  result.mapOrElse(
    async (d) => ok<SuccessType, ErrorType>(await d),
    async () => err(result.unwrapErr())
  )

export { transposeResultToPromise }
