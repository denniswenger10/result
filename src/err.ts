import type { Result } from './result.ts'

const err = <SuccessType, ErrorType>(
  error: ErrorType
): Result<SuccessType, ErrorType> => {
  return {
    isOk: () => false,
    isErr: () => true,
    unwrap: () => {
      throw new Error(
        "No data here. Don't use unwrap unless you know it's completely safe."
      )
    },
    unwrapErr: () => error,
    unwrapOr: (or) => or,
    unwrapOrElse: (orElse) => orElse(),
    match: ({ err: _err }) => _err(error),
    map: () => err(error),
    mapOr: (_, or) => or,
    mapOrElse: (_, orElse) => orElse(),
    mapErr: (fn) => err(fn(error)),
    and: () => err(error),
    andThen: () => err(error),
    or: (result) => result,
    contains: () => false,
    containsErr: (secondError) => error === secondError,
  }
}

export { err }
