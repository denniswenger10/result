import type { Result } from './result.ts'

const ok = <SuccessType, ErrorType>(
  data: SuccessType
): Result<SuccessType, ErrorType> => {
  return {
    isOk: () => true,
    isErr: () => false,
    unwrap: () => data,
    unwrapErr: () => {
      throw new Error(
        "No error here. Don't use unwrapErr unless you know it's completely safe."
      )
    },
    unwrapOr: () => data,
    unwrapOrElse: () => data,
    match: ({ ok: _ok }) => _ok(data),
    map: (fn) => ok(fn(data)),
    mapOr: (fn) => fn(data),
    mapOrElse: (fn) => fn(data),
    mapErr: () => ok(data),
    and: (result) => result,
    andThen: (fn) => fn(data).map((_data) => _data),
    or: () => ok(data),
    contains: (secondData) => data === secondData,
    containsErr: () => false,
  }
}

export { ok }
