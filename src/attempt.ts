import type { Result } from './result.ts'
import { ok } from './ok.ts'
import { err } from './err.ts'

const attempt = <SuccessType>(
  fn: () => SuccessType
): Result<SuccessType, Error> => {
  try {
    return ok(fn())
  } catch (error) {
    return err(error)
  }
}

export { attempt }
