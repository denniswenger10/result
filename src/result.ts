type Result<SuccessType, ErrorType> = {
  unwrap(): SuccessType
  unwrapErr(): ErrorType
  unwrapOr(or: SuccessType): SuccessType
  unwrapOrElse(orElse: () => SuccessType): SuccessType
  isOk(): boolean
  isErr(): boolean
  match<MatchExpressionType>(matchers: {
    ok(data: SuccessType): MatchExpressionType
    err(error: ErrorType): MatchExpressionType
  }): MatchExpressionType
  map<FnReturnType>(
    fn: (data: SuccessType) => FnReturnType
  ): Result<FnReturnType, ErrorType>
  mapOr<FnReturnType>(
    fn: (data: SuccessType) => FnReturnType,
    or: FnReturnType
  ): FnReturnType
  mapOrElse<FnReturnType>(
    fn: (data: SuccessType) => FnReturnType,
    orElse: () => FnReturnType
  ): FnReturnType
  mapErr<FnReturnType>(
    fn: (error: ErrorType) => FnReturnType
  ): Result<SuccessType, FnReturnType>
  and(
    result: Result<SuccessType, ErrorType>
  ): Result<SuccessType, ErrorType>
  andThen(
    fn: (data: SuccessType) => Result<SuccessType, ErrorType>
  ): Result<SuccessType, ErrorType>
  or(
    result: Result<SuccessType, ErrorType>
  ): Result<SuccessType, ErrorType>
  contains(data: SuccessType): boolean
  containsErr(error: ErrorType): boolean
}

export type { Result }
