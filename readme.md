# Result

A library with a small set of functions that should give you the ability to write a program without `undefined` or `null`, which makes your program easier to reason about and safer.

## Installation

This package can be used with both **Node** and **Deno**.

### Node

```bash
npm i @wentools/result
```

### Deno

#### From a dependency file

```typescript
// Preferably in deps.ts
export {
  some,
  none,
} from 'https://gitlab.com/wentools/result/-/raw/v0.1.0/src/mod.ts'
```

#### Consume directly

```typescript
import {
  some,
  none,
} from 'https://gitlab.com/wentools/result/-/raw/v0.1.0/src/mod.ts'
```

## Basic usage


## Maintainers

- [Dennis Wenger](https://gitlab.com/denniswenger10)
